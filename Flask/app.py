from flask import Flask, render_template, request
import pymysql
app = Flask(__name__)


@app.route("/")
def index():
    return render_template("index.html")

@app.route("/1")
def jia():
    return render_template("add.html")

@app.route("/2")
def jian():
    return render_template("sub.html")

@app.route("/3")
def cheng():
    return render_template("mul.html")

@app.route("/4")
def chu():
    return render_template("div.html")


@app.route("/add", methods=['post'])
def add():
    a = request.form.get("a", type=int)
    b = request.form.get("b", type=int)
    conn = pymysql.connect(host="localhost", port=3306, user="root", password="root", database="ahu_2021_test", charset="gbk")

    cursor = conn.cursor()
    affect_rows = cursor.execute(
        f"insert into calc_record(operand1,operand2,result) values('%d','%d','%d')") % (a, b, a + b)
    conn.commit()
    return str(a+b)

@app.route("/sub", methods=['post'])
def sub():
    a = request.form.get("a", type=int)
    b = request.form.get("b", type=int)
    return str(a-b)

@app.route("/mul", methods=['post'])
def mul():
    a = request.form.get("a", type=int)
    b = request.form.get("b", type=int)
    return str(a*b)

@app.route("/div", methods=['post'])
def div():
    a = request.form.get("a", type=int)
    b = request.form.get("b", type=int)
    return str(a/b)

if __name__ == '__main__':
    app.run()

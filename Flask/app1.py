from flask import Flask, render_template, request #导入需要的python库
import pymysql
app = Flask(__name__) #创建一个app应用


@app.route("/")  #默认登录后的界面是index.html，指导如何使用计算器
def index():
    return render_template("index.html")  

@app.route("/1") #进入加法计算界面
def jia():
    return render_template("add.html")

@app.route("/2") #进入减法计算界面
def jian():
    return render_template("sub.html")

@app.route("/3") #进入乘法计算界面
def cheng():
    return render_template("mul.html")

@app.route("/4") #进入除法计算界面
def chu():
    return render_template("div.html")


@app.route("/add", methods=['post']) #开始加法计算
def add():
    a = request.form.get("a", type=int)
    b = request.form.get("b", type=int)
  
    return str(a+b)

@app.route("/sub", methods=['post']) #开始减法计算
def sub():
    a = request.form.get("a", type=int)
    b = request.form.get("b", type=int)
    return str(a-b)

@app.route("/mul", methods=['post']) #开始乘法计算
def mul():
    a = request.form.get("a", type=int)
    b = request.form.get("b", type=int)
    return str(a*b)

@app.route("/div", methods=['post']) #开始除法计算
def div():
    a = request.form.get("a", type=int)
    b = request.form.get("b", type=int)
    return str(a/b)

if __name__ == '__main__':
    app.run(host=0.0.0.0, debug=True, port = 5000)
